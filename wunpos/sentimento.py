

class Sentimento(object):
    def __init__(self, celula):
        self.safes = []
        self.notSafe = []
        self.fedors = []
        self.brisas = []
        self.visitadas = []
        self.possivelMonstro = []
        self.possivelPoco = []
        self.safes.append(celula)
        self.visitadas.append(celula)

    def analisar(self, celula):
        sentimentos = []

        self.addVisita(celula)

        itens = celula.getItens()

        if not self.contem(itens, 'poco') and not self.contem(itens, 'fedor'):
            self.addSafe(celula)

        if self.contem(itens, 'brisa') and self.contem(itens, 'fedor'):
            self.trataBrisaEFedor(celula)

        if not self.contem(itens, 'brisa') and not self.contem(itens, 'fedor'):
            self.vizinhosSafe(celula)
        
        if self.contem(itens, 'brisa'):
            self.trataBrisa(celula)

        if self.contem(itens, 'fedor'):
            self.trataFedor(celula)

        sentimentos.append(self.strVisitadas())
        sentimentos.append(self.strSeguras())
        return sentimentos

    def trataBrisaEFedor(self, celula):
        self.trataBrisa(celula)
        self.trataFedor(celula)

    def isSafe(self, celula):
        return celula in self.safes

    def trataBrisa(self, celula):
        self.addBrisa(celula)

        vizinhas = celula.getVizinhas()

        for v in vizinhas:
            self.verificaBrisaVizinhasVisitadas(v)
                
        # for v in vizinhas:
        #     if(not self.isSafe(v)):
        #         self.addPossivelPoco(v)
        #         if self.isPossivelMostro(v) and not self.contem(celula.itens, 'fedor'):
        #             self.possivelMonstro.remove(v)
        #             self.verificaBrisaVizinhasVisitadas(v)


    def verificaBrisaVizinhasVisitadas(self, celula):
        for v in celula.getVizinhas():
            if v in self.visitadas and v not in self.brisas:
                self.addSafe(celula)
                break


    def trataFedor(self, celula):
        if celula in self.fedors:
            return

        if self.fedors.__len__() == 0:
            self.fedors.append(celula)
            return

        self.fedors.append(celula)

        celulaComfedor = self.fedors[0]
        vizinhas1 = celulaComfedor.getVizinhas()
        vizinhas2 = celula.getVizinhas()

        celulaComMonstro = self.vizinhaEmComum(vizinhas1, vizinhas2)
        self.notSafe.append(celulaComMonstro)
            
    def vizinhaEmComum(self, vizinhas1, vizinhas2):
        for v1 in vizinhas1:
            for v2 in vizinhas2:
                if v1 == v2:
                    return v1

        return None        

    def vizinhosSafe(self, celula):
        for v in celula.getVizinhas():
            self.addSafe(v)

    def addSafe(self, celula):
        if celula not in self.safes:
            self.safes.append(celula)

    def addNotSafe(self, celula):
        if celula not in self.notSafe:
            self.notSafe.append(celula)

    def addBrisa(self, celula):
        if celula not in self.brisas:
            self.brisas.append(celula)

    def addFedor(self, celula):
        if celula not in self.fedors:
            self.fedors.append(celula)

    def addPossivelPoco(self, celula):
        if celula not in self.possivelPoco:
            self.possivelPoco.append(celula)

    def contem(self, itens, nomeItem):
        for item in itens:
            if item.nome() == nomeItem:
                return True
        return False

    def addVisita(self, celula):
        if celula not in self.visitadas:
            self.visitadas.append(celula)


    def isPossivelMostro(self, celula):
        return celula in self.possivelMonstro

    def isPossivelPoco(self, celula):
        return celula in self.possivelPoco

    def strVisitadas(self):
        string = "visitadas: "
        for v in self.visitadas:
            string = string+v.__str__() +","
        
        return string

    def strSeguras(self):
        string = "seguras: "
        for v in self.safes:
            string = string+v.__str__() +","
        
        return string
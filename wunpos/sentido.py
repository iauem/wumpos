
class Sentido(object):
    def __init__(self):
        self.sentido = Sul()

    def proximo(self):
        self.sentido = self.sentido.proximo()
        return self.sentido

    def descricao(self):
        return self.sentido.descricao()

    def getProximaCelula(self, celula):
        return self.sentido.getProximaCelula(celula)

    def temProximaCelula(self, celula):
        return  self.sentido.temProximaCelula(celula)


class Sul:
    def proximo(self):
        return Oeste()

    def descricao(self):
        return 'sul'

    def getProximaCelula(self, celula):
        return celula.vizinhoDeBaixo()

    def temProximaCelula(self, celula):
        return  celula.temVizinhoAbaixo()


class Oeste:
    def proximo(self):
        return Norte()

    def descricao(self):
        return 'oeste'

    def getProximaCelula(self, celula):
        return celula.vizinhoDaEsquerda()

    def temProximaCelula(self, celula):
        return  celula.temVizinhoAEsquerda()

class Norte:
    def proximo(self):
        return Leste()

    def descricao(self):
        return 'norte'

    def getProximaCelula(self, celula):
        return celula.vizinhoDeCima()

    def temProximaCelula(self, celula):
        return  celula.temVizinhoAcima()

class Leste:
    def proximo(self):
        return Sul()

    def descricao(self):
        return 'leste'

    def getProximaCelula(self, celula):
        return celula.vizinhoDaDireita()

    def temProximaCelula(self, celula):
        return  celula.temVizinhoADireita()
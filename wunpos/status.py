
from sentido import Sul, Norte, Leste, Oeste

class StatusCelula(object):
    def __init__(self, anterior, proximo):
        self.anterior = anterior
        self.proximo = proximo

    def acaoInversa(self, agente, caminho):
        a = agente.getSentido().descricao()
        b =  self.getSentidoDesejado().descricao()
        if agente.getSentido().descricao() == self.getSentidoDesejado().descricao():
            agente.andar()
            caminho.pop()
        else:
            agente.girar()
            


    def getSentidoDesejado(self):
        if self.proximo.temVizinhoAcima() and self.proximo.vizinhoDeCima() == self.anterior:
            return Norte()

        if self.proximo.temVizinhoAbaixo() and self.proximo.vizinhoDeBaixo() == self.anterior:
            return Sul()

        if self.proximo.temVizinhoADireita() and self.proximo.vizinhoDaDireita() == self.anterior:
            return Leste()

        if self.proximo.temVizinhoAEsquerda() and self.proximo.vizinhoDaEsquerda() == self.anterior:
            return Oeste()

class StatusSentido(object):
    def __init__(self, anterior, proximo):
        self.anterior = anterior
        self.proximo = proximo

    def acaoInversa(self, agente):
        agente.setSentido(self.anterior)
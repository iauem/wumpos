""" Classe Celula """

class Celula(object):
    """classe celula"""
    def __init__(self, mundo, posicao):
        self.posicao = posicao
        self.mundo = mundo
        self.itens = []
    

    def getPosicao(self):
        return self.posicao

    def addItem(self, item):
        self.itens.append(item)
        item.acaoInicial()

    def removeOuro(self, ouro):
        self.itens.remove(ouro)

    def removeAgente(self, agente):
        self.itens.remove(agente)

    def recebeAgente(self, agente):
        for item in self.itens:
            item.notifica(agente)

        self.itens.append(agente)

    def getItens(self):
        return self.itens

    def getVizinhas(self):
        vizinhos = []

        if self.temVizinhoAbaixo():
            celula = self.vizinhoDeBaixo()
            vizinhos.append(celula)

        if self.temVizinhoAcima():
            celula = self.vizinhoDeCima()
            vizinhos.append(celula)

        if self.temVizinhoAEsquerda():
            celula = self.vizinhoDaEsquerda()
            vizinhos.append(celula)

        if self.temVizinhoADireita():
            celula = self.vizinhoDaDireita()
            vizinhos.append(celula);

        return vizinhos

    def vizinhoDaEsquerda(self):
        return self.mundo.getCelula(self.posicao.getX(), self.posicao.getY() - 1)
        
    def vizinhoDaDireita(self):
        return self.mundo.getCelula(self.posicao.getX(), self.posicao.getY() + 1);

    def vizinhoDeCima(self):
        return self.mundo.getCelula(self.posicao.getX() - 1, self.posicao.getY())

    def vizinhoDeBaixo(self):
        return self.mundo.getCelula(self.posicao.getX() + 1, self.posicao.getY())


    def temVizinhoAbaixo(self):
         return self.posicao.getX() < 3

    def temVizinhoAcima(self):
         return self.posicao.getX() > 0

    def temVizinhoAEsquerda(self):
         return self.posicao.getY() > 0

    def temVizinhoADireita(self):
         return self.posicao.getY() < 3

    def celulaAbaixo(self):
        return self.vizinhoDeBaixo()

    def getImagem(self):
        if self.itens.__len__() == 0:
            return 'vazio'

        if self.itensContains('agente'):
            return self.getItem('agente').getImagem()

        if self.itensContains('ouro'):
            return 'ouro'

        if self.itensContains('monstro'):
            return 'monstro'

        if self.itensContains('poco'):
            return 'poco'

        if self.itensContains('fedor') and self.itensContains('brisa'):
            return 'fedor-brisa'

        if self.itensContains('fedor'):
            return 'fedor'

        if self.itensContains('brisa'):
            return 'brisa'

        return 'vazio'


    def itensContains(self, caracteristica):
        for item in self.itens:
            if item.nome() == caracteristica:
                return True

        return False

    def getItem(self, nomeItem):
        for  item in self.itens:
            if item.nome() == nomeItem:
                return item
        
        return None

    def __str__(self):
        x = self.posicao.getX()
        y = self.posicao.getY()
        return "("+str(x)+","+str(y)+")"
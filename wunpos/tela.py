from appJar import gui
from mundo import Mundo

app = gui("Mundo de Wumpos", "1000x600")

mundo = Mundo()

def press(btn):
    print(btn)

def iniciarJogo(btn):
    mundo.iniciar()
    reloadImagens()
    imprimeSaida("Jogo Iniciado")
    agente = mundo.getAgente()
    print(agente)

def getAgente():
    return mundo.getAgente()

def passo(btn):
    print("andando")
    sentimentos = getAgente().agir()
    reloadImagens()

    for s in sentimentos:
        imprimeSaida(s)


def createLabels(app):
    for indexX in range(4):
        for indexY in range(4):
            count = str(indexX)+str(indexY)
            createLabel(app, count, indexX, indexY)

def createLabel(app, indexFrame, indexX, indexY):
    app.startLabelFrame
    app.startLabelFrame(indexFrame, indexX, indexY)
    app.addImage("bg"+str(indexFrame), "img/vazio.gif")
    app.stopLabelFrame()

def reloadImagens():
    for x in range(4):
        for y in range(4):
            setImage(mundo.getCelula(x,y))

def setImage(celula):
    imgId = "bg"+str(celula.getPosicao().getX())+str(celula.getPosicao().getY())
    if celula.getImagem() == 'agente-vencedor':
        app.reloadImage(imgId, "img/"+celula.getImagem()+".gif")
    else:
        app.reloadImage(imgId, "img/"+celula.getImagem()+".gif")

def imprimeSaida(texto):
    antigo = app.getTextArea("saida")
    app.clearTextArea("saida")
    app.setTextArea("saida", antigo+"\n"+texto+"\n\n")      

# Mundo
app.startPanedFrame("mundo",0,0)

createLabels(app)

app.reloadImage("bg00", "img/sul.gif")

app.stopPanedFrame()
#Fim do mundo

# start second, vertical pane inside initial pane
app.startPanedFrame("p2",0,1)

app.addLabel("nomeTrabalho", "Mundo do Wumpus")


app.addButton("Iniciar", iniciarJogo, 1, 0)
app.addButton("Passo", passo, 1, 1 )

app.addTextArea("saida", 2, 0, 4)
#app.addScrolledTextArea("saida", 2, 0, 4)
app.setTextArea("saida", "Inicie o Jogo.")
app.stopPanedFrame()

app.go()

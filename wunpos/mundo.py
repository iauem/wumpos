from celula import Celula
from posicao import Posicao
from poco import Poco
from monstro import Monstro
from agente import Agente
from sorteio import Sorteio

class Mundo:
    """classe Mundo"""
    def __init__(self):
        self.celulas = [[0 for x in range(4)] for y in range(4)]

    def iniciar(self):
        self.definirCelulas()
        sorteio = Sorteio(self.celulas)
        sorteio.sorteia()
        print("Jogo iniciado")

        celulaAgente = self.celulas[0][0]
        Agente(celulaAgente)


    def definirCelulas(self):
        for linha in range(4):
            for coluna in range(4):
                self.celulas[linha][coluna] = Celula(self, Posicao(linha, coluna))


    def getCelulas(self):
        return self.celulas

    def getCelula(self, x, y):
        return self.celulas[x][y]

    def getAgente(self):
        for linha in range(4):
            for coluna in range(4):
                for item in self.celulas[linha][coluna].getItens():
                    if(item.nome() == 'agente'):
                        return item
        return None
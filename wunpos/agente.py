from sentido import Sentido
from sentimento import Sentimento
from status import StatusCelula, StatusSentido

class Agente(object):
    def __init__(self, celula):
        self.celula = celula
        self.celula.addItem(self)
        self.sentido = Sentido()
        self.vivo = True
        self.sentimento = Sentimento(self.celula)
        self.giros = 0
        self.pegouOuro = False
        self.caminho = []
        self.perigos = []
        self.primeiraAcao = True
        self.jaGirou = {'sul': False, 'oeste': False, 'leste': False, 'norte': False} 

    def acaoInicial(self):
        pass

    def nome(self):
        return 'agente'

    def getImagem(self):
        if self.vivo:
            if self.isVencedor():
                return 'agente-vencedor'

            return self.sentido.descricao()
        
        return 'agente-morto'

    def agir(self):

        if self.primeiraAcao:
            self.primeiraAcao = False
            sentimento = self.sentir()
            self.andar()
            return sentimento

        if self.isVencedor():
            return []

        if self.pegouOuro:
            tam = self.caminho.__len__()
            status = self.caminho.__getitem__(tam - 1)
            status.acaoInversa(self, self.caminho)
            return []
        
        if not self.sentido.temProximaCelula(self.celula): 
            self.girar()
            return []

        sentimento = self.sentir()
        if self.sentimento.isSafe(self.sentido.getProximaCelula(self.celula)) or self.giros > 1:
            # se arrisca self.giros > 1
            self.andar()
        else:
            # proximaCelula = self.sentido.getProximaCelula(self.celula)
            # proximaCelula = self.sentido.getProximaCelula(self.celula)
            # sentimentoCelulas = self.sentimento
            # print(proximaCelula)
            # print(sentimento)
            # print(sentimentoCelulas)
            if self.giros > 2:
                self.andar()
            else:
                # aqui eh o lugar pra colocar a logica de se arriscar
                orientacao = self.getSentido().descricao()
                jaGirou = self.jaGirou[orientacao]
                if self.celula in self.sentimento.visitadas and jaGirou:
                    self.andar()
                else:
                    self.jaGirou[orientacao] = True
                    self.girar()
        
        return sentimento
        
    def sentir(self):
        return self.sentimento.analisar(self.celula)

    def andar(self):
        proximaCelula = self.sentido.getProximaCelula(self.celula)

        if not self.pegouOuro:
            self.caminho.append(StatusCelula(self.celula, proximaCelula))
        
        self.celula.removeAgente(self)
        self.celulaAnterior = self.celula

        self.celula = proximaCelula
        self.celula.recebeAgente(self)
        self.giros = 0

    def girar(self):
        self.giros = self.giros + 1
        anterior = self.sentido
        self.sentido.proximo()
        proximo = self.sentido

    def morrer(self):
        self.vivo = False

    def pegaOuro(self):
        print("pegou ouro<<<<<<<<<<<<<<<<<<<<")
        self.pegouOuro = True

    def setSentido(self, sentido):
        self.sentido = sentido

    def getSentido(self):
        return self.sentido

    def estouNaCelulaInicial(self):
        pos = self.celula.getPosicao()
        return pos.getX() == 0 and pos.getY() == 0

    def isVencedor(self):
        return self.pegouOuro and self.estouNaCelulaInicial()
from brilho import Brilho

class Ouro(object):
    def __init__(self, celula):
        self.celula = celula
        self.celula.addItem(self)

    def acaoInicial(self):
        vizinhas = self.celula.getVizinhas()
        for v in vizinhas:
            Brilho(v)

    def getPosicao(self):
        return self.celula.getPosicao()

    def nome(self):
        return 'ouro'

    def notifica(self, agente):
        agente.pegaOuro()
        self.celula.removeOuro(self)

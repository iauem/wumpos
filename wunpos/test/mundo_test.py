import unittest

from wunpos.mundo import Mundo
from wunpos.poco import Poco
from wunpos.monstro import Monstro

class MundoTest(unittest.TestCase):
    def testCriaMundo(self):
        mundo = Mundo()
        mundo.iniciar()

        self.assertEqual(4, mundo.getCelulas().__len__())
        self.assertEqual(4, mundo.getCelulas()[0].__len__())

    def testBrisaAoRedorDoPoco(self):
        mundo = Mundo()
        mundo.iniciar()

        celula = self.getCelula(mundo, 'poco')

        vizinhas = celula.getVizinhas()
        for v in vizinhas:
            itens = v.getItens()
            self.assertTrue(self.itensContem(itens, 'brisa'))

    

    def testFedorAoRedorDoMonstro(self):
        mundo = Mundo()
        mundo.iniciar()

        celula = self.getCelula(mundo, 'monstro')

        vizinhas = celula.getVizinhas()
        for v in vizinhas:
            itens = v.getItens()
            self.assertTrue(self.itensContem(itens, 'fedor'))

        

    def getCelula(self, mundo, nome):
        celulas = mundo.getCelulas()
        for linha in range(4):
            for colula in range(4):
                celula = celulas[linha][colula]
                itens = celula.getItens()
                if(self.itensContem(itens, nome)):
                    return celula
        return None

    def itensContem(self, itens, nome):
        for item in itens:
            if item.nome() == nome:
                return True
        return False

        
if __name__ == '__main__':
    unittest.main()
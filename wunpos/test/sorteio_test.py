import unittest

from wunpos.celula import Celula
from wunpos.posicao import Posicao
from wunpos.sorteio import Sorteio
from wunpos.mundo import Mundo
# from wunpos.monstro import Monstro

class SorteioTest(unittest.TestCase):
    """sorteioTest"""
    def test_matrixToArray(self):
        """Testa conversao da matrix de celulas para array e os metodos utilitarios usados no sorteia"""
 
        celulas = self.criaCelulas()
        sorteio = Sorteio(celulas)
        array = sorteio.getArray()
        # testa se matrixToArray foi correto na conversao.
        # matrixToArray foi invocado no construtor de Sorteio
        for i in range(4):
            for j in range(4):
                self.assertEqual(array[(i*4)+j], celulas[i][j])
        
        # testa limites para removeCelula
        celula1 = array[0]
        sorteio.removeCelula(0)
        novoArray = sorteio.getArray()
        
        self.assertEqual(len(novoArray), 15)
        for i in range(15):
            self.assertNotEqual(novoArray[i], celula1)
        
        celula14 = novoArray[14]
        sorteio.removeCelula(14)
        novoArray = sorteio.getArray()
        for i in range(14):
            self.assertNotEqual(novoArray[i], celula14)

    def test_sorteio(self):
        
        celulas = self.criaCelulas()
        celulaAgente = celulas[0][0]
        sorteio = Sorteio(celulas)
        sorteio.sorteia()

        self.assertEqual(11, len(sorteio.getArray()))
        

    def criaCelulas(self):
        m = Mundo()
        m.iniciar()
        return m.getCelulas()
        # celulas = [[0 for x in range(4)] for y in range(4)]
        # for linha in range(4):
        #     for coluna in range(4):
        #         celulas[linha][coluna] = Celula(self, Posicao(linha, coluna))

        # return celulas

if __name__ == '__main__':
    unittest.main()



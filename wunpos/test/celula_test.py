import unittest

from wunpos.celula import Celula
from wunpos.posicao import Posicao

class CelulaTest(unittest.TestCase):
    """celulaTest"""
    def test_celula_canto0(self):
        """Testa celula do canto"""
        celula = Celula(None,Posicao(0,0))
        self.assertEqual(celula.temVizinhoAbaixo(), True)
        self.assertEqual(celula.temVizinhoAcima(), False)
        self.assertEqual(celula.temVizinhoAEsquerda(), False)
        self.assertEqual(celula.temVizinhoADireita(), True)

    def test_celula_canto1(self):
        celula = Celula(None,Posicao(3,0))
        self.assertEqual(celula.temVizinhoAbaixo(), True)
        self.assertEqual(celula.temVizinhoAcima(), False)
        self.assertEqual(celula.temVizinhoAEsquerda(), True)
        self.assertEqual(celula.temVizinhoADireita(), False)

    def test_celula_canto2(self):
        celula = Celula(None,Posicao(3,3))
        self.assertEqual(celula.temVizinhoAbaixo(), False)
        self.assertEqual(celula.temVizinhoAcima(), True)
        self.assertEqual(celula.temVizinhoAEsquerda(), True)
        self.assertEqual(celula.temVizinhoADireita(), False)

    def test_celula_canto3(self):
        celula = Celula(None,Posicao(0,3))
        self.assertEqual(celula.temVizinhoAbaixo(), False)
        self.assertEqual(celula.temVizinhoAcima(), True)
        self.assertEqual(celula.temVizinhoAEsquerda(), False)
        self.assertEqual(celula.temVizinhoADireita(), True)


if __name__ == '__main__':
    unittest.main()



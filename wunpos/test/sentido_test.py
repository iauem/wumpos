import unittest

from wunpos.sentido import Sentido

class SentidoTest(unittest.TestCase):
    def testProximo(self):
        sentido = Sentido()
        self.assertEqual("sul", sentido.descricao())

        sentido.proximo()
        self.assertEqual("oeste", sentido.descricao())

        sentido.proximo()
        self.assertEqual("norte", sentido.descricao())

        sentido.proximo()
        self.assertEqual("leste", sentido.descricao())

        sentido.proximo()
        self.assertEqual("sul", sentido.descricao())


if __name__ == '__main__':
    unittest.main()
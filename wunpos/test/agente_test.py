import unittest
import sys


from wunpos.mundo import Mundo

class AgenteTest(unittest.TestCase):
    def testAndar(self):
        mundo = Mundo()
        mundo.iniciar()
        celula00 = mundo.getCelula(0,0)

        itens = celula00.getItens()
        self.assertTrue(self.itensContemAgente(itens))

        agente = self.getAgente(itens)
        agente.andar()

        itens = celula00.getItens()
        self.assertFalse(self.itensContemAgente(itens))

        celula01 = mundo.getCelula(0,1)
        itens = celula01.getItens()
        self.assertTrue(self.itensContemAgente(itens))

    def itensContemAgente(self, itens):
        for item in itens:
            if item.nome() == 'agente':
                return True
        
        return False

    def getAgente(self, itens):
        for item in itens:
            if item.nome() == 'agente':
                return item
        
        return None

if __name__ == '__main__':
    unittest.main()

""" Classe Posicao """

class Posicao:
    def __init__(self, x, y):
        if (x < 0 or y < 0):
            raise Exception('X e Y devem ser maiores que zero')
        if(x > 3 or y > 3):
            raise Exception('X e Y devem ser menores que tres')

        self.x = x
        self.y = y


    def getX(self):
        return self.x

    def getY(self):
        return self.y

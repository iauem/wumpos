
from brisa import Brisa

class Poco(object):
    def __init__(self, celula):
        self.celula = celula
        self.celula.addItem(self)

    def acaoInicial(self):
        vizinhas = self.celula.getVizinhas()
        for v in vizinhas:
            Brisa(v)

    def getPosicao(self):
        return self.celula.getPosicao()

    def nome(self):
        return 'poco'

    def notifica(self, agente):
        agente.morrer()
        
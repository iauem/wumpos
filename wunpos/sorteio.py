""" Classe Sorteio """
import random
from monstro import Monstro
from poco import Poco
from ouro import Ouro

class Sorteio(object):
    """classe sorteio"""
    def __init__(self, celulas):
        self.celulas = celulas   
        self.matrixToArray()

    def sorteia(self):
        self.matrixToArray()
        self.removeCelula(0) #remove o lugar onde o agente nasce.

        self.adicionaElemento(Monstro)
        self.adicionaElemento(Poco)
        self.adicionaElemento(Poco)
        self.adicionaElemento(Poco) 
        self.adicionaElemento(Ouro)

        # Poco(self.array[1])
        # Monstro(self.array[7])
        # Ouro(self.array[8])
        # Poco(self.array[9])
        # Poco(self.array[14])





    def matrixToArray(self):
        self.array = []
        for i in range(4):
            for j in range(4):
                self.array.append(self.celulas[i][j])
        
        return self.array
    
    def getArray(self):
        return self.array

    def adicionaElemento(self, elemento):
        posicao = random.randint(0, len(self.array) - 1)
        celula = self.array[posicao]
        elemento(celula)
        self.removeCelula(posicao)

    def removeCelula(self, posicao):
        self.array = self.array[:posicao] + self.array[posicao+1:]
        
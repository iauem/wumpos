from fedor import Fedor

class Monstro(object):
    def __init__(self, celula):
        self.celula = celula
        self.celula.addItem(self)

    def acaoInicial(self):
        vizinhas = self.celula.getVizinhas()
        for v in vizinhas:
            Fedor(v)

    def getPosicao(self):
        return self.celula.getPosicao()

    def nome(self):
        return 'monstro'

    def notifica(self, agente):
        agente.morrer()
